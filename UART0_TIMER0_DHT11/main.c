/*
 * UART0_TIMER0_DHT11.c
 *
 * Created: 2020-08-07 오전 10:36:11
 * Author : kccistc
 */ 
#define F_CPU 16000000UL
#include <avr/io.h>
#include <util/delay.h>
#include <string.h>
#include <stdio.h>
#include <avr/interrupt.h>

#include "uart0.h"		// UART0 헤더
#include "DHT.h"		// DHT11 헤더

// UART0 USART0_RX_vect가 호출 한다.
extern volatile int i;									// buffer 의 index 변수
extern volatile unsigned char buffer[MAX_QSIZE];		// UART 부터 읽어온 data를 \r 이나 \n을 만날때 까지 저장할 공간
extern volatile int rxReadyFlag;						// \r이나 \n을 만났다는 인디케이터 flag변수
// UART0로 부터 1byte가 들어오면, 인터럽트가 발생되어 이곳으로 들어온다.
// P278 표 12-3 참고
ISR(USART0_RX_vect){
	UART0_ISR_Receive();	// ISR 내에서 CALL하는건 바람직하지 않다.
}

volatile int count = 0;
// TIMER0 OVF INTERRUPT
// 16Mkhz --> 1024분주 시 16kHz --> 256 count 시 16ms마다 진입

ISR(TIMER0_OVF_vect){
	count++;
}

FILE OUTPUT = FDEV_SETUP_STREAM(UART0_transmit, NULL, _FDEV_SETUP_WRITE);
// UART 함수를 OUTPUT FILE POINTER 로 MAPPING
void UART0_printf_test();
void UART0_tx_rx_test();
void UART0_string_receive_transmit_test();
void UART0_rx_interrupt_tx_polling_test();
void UART0_DHT11_test();

// rx는 polling 으로 수행시 데이터 누락가능성 ( 반드시 rx인터럽트로 짤것 )
// 1ms : 9600 bps
// 0.2ms : 115200 bps

int main(void)
{
    /* Replace with your application code */
	// led
	DDRA = 0xff;

	// lcd
	DDRC = 0xff;		// RS, RW, E
	DDRF = 0xff;		// DATA
	LCD_init();
	LCD_str("HELLO WORLD 8");

	timer0_init();
//	test();				// 1번문제
	test2();			// 2번문제
		
	while (1) 
    {
    }
}

void MY_DHT_Read(unsigned char* temp, unsigned char* hum ){
	unsigned char buffer[5] = { 0, 0, 0, 0, 0 };
	
	DDRG = 0xff;
	PORTG = 0x00;
	_delay_ms(20);
	
	DDRG = 0x00;
	PORTG = 0xff;
	
	while((PING & 0x01));		// wait 0
	
	while(!(PING & 0x01));		// wait 1
	
	while((PING & 0x01));		// wait 0
	
	for(int i=0;i<5;i++){
		for(int j=0;j<8;j++){
			while(!(PING & 0x01));		// wait 1
			
			_delay_us(35);
			if(PING & 0x01)
				buffer[i] |= 1<<(7-j);
				
			while(PING & 0x01);			// wait 0
		}
	}
	
	hum[0] = buffer[0];
	hum[1] = buffer[1];
	temp[0] = buffer[2];
	temp[1] = buffer[3];
}

void test2(){
		int dhtonoff = 0;			// 1 : on,  0 : off
		
		unsigned char * receivedData;
		unsigned char temp[2] = {0xa5, 0xa5};				// 온도 정보를 읽어들일 buff
		unsigned char hum[2] = {0x5a, 0x5a};				// 습도 정보를 읽어들일 buff
		
		UART0_init();
		stdout = &OUTPUT;
		sei();
		_delay_ms(2000);				// 안정화 시간 2초
		printf("uart0 RX <--> TX string test !!!!!!\n");
		
		while(1){
			if(rxReadyFlag == 1){		// 완전한 message 가 들어왔는가
				receivedData = getRxString();
							
				if(strncmp(buffer, "dht11on", sizeof("dht11on")) == 0){
					// dht11on 시키는 코드 삽입
					PORTA |= 0b00000010;				// 0번 LED를 ON
					printf("dht11on...\n");
					LCD_clear();
					dhtonoff = 1;
				}else if(strncmp(buffer, "dht11off", sizeof("dht11off")) == 0){
					// dht11off 시키는 코드 삽입
					PORTA &= 0b11111101;				// 0번 LED를 ON
					printf("dht11off..\n");
					LCD_clear();
					dhtonoff = 0;
				}
			}
			
			if(count >= 125){		// 16ms x 125 == 2000ms (2초)
				count = 0;
				if(dhtonoff){
					MY_DHT_Read(temp, hum);				// 온습도 센서 제어 코드
					printf("[TEMP] %d.%d\n", temp[0], temp[1]);
					printf("[HUMI] %d.%d\n", hum[0], hum[1]);

					char tmp[20];
					sprintf(tmp, "[TEMP] %2d.%2d", temp[0], temp[1]);
					LCD_xy(0,0);
					LCD_str(tmp);
					sprintf(tmp, "[HUMI] %2d.%2d", hum[0], hum[1]);
					LCD_xy(1,0);
					LCD_str(tmp);
				}
			}
			
			_delay_ms(2000);			// 안정화기간 2s
		}
}

void test1(){
		int dhtonoff = 0;			// 1 : on,  0 : off
	
		unsigned char * receivedData;
		unsigned char temp[2];				// 온도 정보를 읽어들일 buff
		unsigned char hum[2];				// 습도 정보를 읽어들일 buff
		
		UART0_init();
		stdout = &OUTPUT;
		sei();
		DHT_Setup();		// 안정화 시간 2초
		printf("uart0 RX <--> TX string test !!!!!!\n");
		
		while(1){
			if(rxReadyFlag == 1){		// 완전한 message 가 들어왔는가
				receivedData = getRxString();
				
				if(strncmp(buffer, "dht11on", sizeof("dht11on")) == 0){
					// dht11on 시키는 코드 삽입
					PORTA |= 0b00000010;				// 0번 LED를 ON
					printf("dht11on...\n");
					LCD_clear();
					dhtonoff = 1;
				}else if(strncmp(buffer, "dht11off", sizeof("dht11off")) == 0){
					// dht11off 시키는 코드 삽입
					PORTA &= 0b11111101;				// 0번 LED를 ON
					printf("dht11off..\n");
					LCD_clear();
					dhtonoff = 0;
				}
			}
			
			if(count >= 125){		// 16ms x 125 == 2000ms (2초)
				count = 0;
				if(dhtonoff){
					DHT_Read(temp, hum);		// DHC.c에 들어있다.
					printf("[TEMP] %d.%d\n", temp[0], temp[1]);
					printf("[HUMI] %d.%d\n", hum[0], hum[1]);
									
					char tmp[20];
					sprintf(tmp, "[TEMP] %2d.%2d", temp[0], temp[1]);
					LCD_xy(0,0);
					LCD_str(tmp);
					sprintf(tmp, "[HUMI] %2d.%2d", hum[0], hum[1]);
					LCD_xy(1,0);
					LCD_str(tmp);
				}
			}
			
			_delay_ms(2000);			// 안정화기간 2s
		}
}

// 온습도(DHT11) 센서에서 값 읽고 UART0 에 출력
void UART0_DHT11_test(){
	unsigned char * receivedData;
	unsigned char temp[2];				// 온도 정보를 읽어들일 buff
	unsigned char hum[2];				// 습도 정보를 읽어들일 buff	
	
	UART0_init();
	stdout = &OUTPUT;
	sei();
	DHT_Setup();		// 안정화 시간 2초
	printf("uart0 RX <--> TX string test !!!!!!\n");
	
	while(1){
		if(rxReadyFlag == 1){		// 완전한 message 가 들어왔는가
			receivedData = getRxString();
			
			if(strncmp(buffer, "led1on", sizeof("led1on")) == 0){
				// led on 시키는 코드 삽입
				PORTA |= 0b00000010;				// 0번 LED를 ON
				printf("led1on..\n");
				LCD_xy(0,0);
				LCD_str("LED1 ON....");
			}else if(strncmp(buffer, "led1off", sizeof("led1off")) == 0){
				// led on 시키는 코드 삽입
				PORTA &= 0b11111101;				// 0번 LED를 ON
				printf("led1off..\n");
				LCD_xy(0,0);
				LCD_str("LED1 OFF...");
			}
		}
		if(count >= 125){		// 16ms x 125 == 2000ms (2초)
			count = 0;
			DHT_Read(temp, hum);		// DHC.c에 들어있다.
			printf("[TEMP] %d.%d\n", temp[0], temp[1]);
			printf("[HUMI] %d.%d\n", hum[0], hum[1]);
						
			char tmp[20];
			sprintf(tmp, "[TEMP] %2d.%2d", temp[0], temp[1]);
			LCD_xy(0,0);
			LCD_str(tmp);
			sprintf(tmp, "[HUMI] %2d.%2d", hum[0], hum[1]);
			LCD_xy(1,0);
			LCD_str(tmp);
		}
		
		_delay_ms(2000);			// 안정화기간 2s
	}
}

/*
	uart0 로부터 수신되는 것은 interrupt로 처리하고
	보내는 것은 polling 방식으로 처리	
*/
void UART0_rx_interrupt_tx_polling_test(){
	unsigned char * receivedData;
	
	UART0_init();
	stdout = &OUTPUT;
	sei();
	
	printf("uart0 RX <--> TX string test !!!!!!\n");
	while(1){
		if(rxReadyFlag == 1){		// 완전한 message 가 들어왔는가
			receivedData = getRxString();
			
			if(strncmp(buffer, "led1on", sizeof("led1on")) == 0){
				// led on 시키는 코드 삽입
				PORTA |= 0b00000010;				// 0번 LED를 ON
				printf("led1on..\n");
				LCD_xy(0,0);
				LCD_str("LED1 ON....");
			}else if(strncmp(buffer, "led1off", sizeof("led1off")) == 0){
				// led on 시키는 코드 삽입
				PORTA &= 0b11111101;				// 0번 LED를 ON
				printf("led1off..\n");
				LCD_xy(0,0);
				LCD_str("LED1 OFF...");
			}
			
//			rxReadyFlag = 0;
		}		
	}
}

/*
	UART0 로부터 string 단위로 rx polling
*/
void UART0_string_receive_transmit_test(){
	unsigned char data;				// data from rx
	unsigned char buffer[100];		// \r \n 를 만날때까지 data 저장하는 수신버퍼
	int i = 0;					// buffer index
	int end_message=0;			// \r \n 만날시 1로 세팅
	
	UART0_init();
	stdout = &OUTPUT;
	
	// 지금부터는 printf 가능
//	UART0_printf_string("uart0 RX --> TX string test !!\n");
	printf("uart0 RX --> TX string test !!\n");
	
	while(1){
//		UART0_transmit(UART0_receive());
		data = UART0_receive();		// uart0 에서 1바이트 읽어서 data 변수에 보관
		if(data == NULL){							// 데이터 없으면 do nothing
			continue;
		}else if(data == '\r' || data == '\n'){		// enter 키 확인되면
			buffer[i] = '\0';						// 종료문자 nul 추가
			end_message = 1;	
		}else{
			buffer[i++] = data;						// 읽어온 byte를 buffer에 저장
		}
		
		if(end_message == 1){					// 완전한 메시지를 받았다면
			if(strncmp(buffer, "led1on", sizeof("led1on")) == 0){				
				// led on 시키는 코드 삽입
				PORTA |= 0b00000010;				// 0번 LED를 ON
				printf("led1on..\n");
				LCD_xy(0,0);
				LCD_str("LED1 ON....");
			}else if(strncmp(buffer, "led1off", sizeof("led1off")) == 0){
				// led on 시키는 코드 삽입
				PORTA &= 0b11111101;				// 0번 LED를 ON
				printf("led1off..\n");				
				LCD_xy(0,0);
				LCD_str("LED1 OFF...");
				
			}
			i = end_message = 0;
		}
	}
}

/*
	UART0 byte 단위로 수신한것으로 송신하는것 (loopback test)
	polling 방식
*/
void UART0_tx_rx_test(){
	UART0_init();

	// led 
	DDRA = 0xff;

	// lcd
	DDRC = 0xff;		// RS, RW, E
	DDRF = 0xff;		// DATA
	LCD_init();
	LCD_str("HELLO WORLD");
	UART0_printf_string("HELLO WORLD");
		
	while (1)
	{
		UART0_transmit(UART0_receive());	// UART0_receive --> UART0_transmit
	}
}

/*
	UART0 로 PRINTF를 하는 시험
*/
void UART0_printf_test(){
	UART0_init();

	// lcd
	DDRC = 0xff;		// RS, RW, E
	DDRF = 0xff;		// DATA
	LCD_init();		
	LCD_str("HELLO WORLD");
	
    while (1)
    {
		UART0_printf_string("HELLO WORLD");
		_delay_ms(1000);
    }
}