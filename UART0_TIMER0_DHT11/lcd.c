﻿/*
 * lcd.c
 *
 * Created: 2020-08-07 오전 11:47:49
 *  Author: kccistc
 */ 
//// lcd init start
#include <avr/io.h>
#include <util/delay.h>

#define RS		0
#define RW		1
#define EN		2

#define LCD_data_port		PORTF
#define LCD_com_port		PORTC

void LCD_com(unsigned char cmd){
	LCD_data_port = cmd;
	LCD_com_port &= ~(1<<RS);	// RS=0 command reg.
	LCD_com_port &= ~(1<<RW);	// RW=0 Write operation
	
	LCD_com_port |= (1<<EN);	// Enable pulse EN=1
	_delay_ms(10);
	LCD_com_port &= ~(1<<EN);	// EN=0
	_delay_ms(5);
}

void LCD_data(unsigned char c_data){
	LCD_data_port = c_data;
	LCD_com_port |= (1<<RS);	// RS=1 Data reg
	LCD_com_port &= ~(1<<RW);	// RW=0 write operation
	
	LCD_com_port |= (1<<EN);	// Enable Pulse
	_delay_ms(10);
	LCD_com_port &= ~(1<<EN);	// EN=0
	_delay_ms(5);
}

void LCD_init(void){
	//	LCD_com_ddr = 0xFF;			// LCD command port out
	//	LCD_data_ddr = 0xFF;		// LCD data port out

	_delay_ms(20);		// 15ms
	LCD_com(0x38);		// Initialization of 16X2 LCD in 8bit mode
	LCD_com(0x0C);		// Display ON Cursor OFF
	LCD_com(0x06);		// Auto increment cursor
	LCD_com(0x01);		// clear display
	LCD_com(0x80);		// cursor at home position
}

void LCD_char(unsigned char c){
	LCD_data(c);		// 문자하나를 출력
	_delay_ms(1);
}

void LCD_str(unsigned char *str){		// 문자열 출력
	while(*str != 0){					// 널문자(\0) 가 있을때가지 반복
		LCD_data(*str);					// 문자 하나를 출력
		str++;							// str의 다음문자 선택
	}
}

void LCD_string(unsigned char *str){
	int i;
	for(i=0;str[i]!=0;i++){		// 널문자(\0) 가 있을때까지 반복
		LCD_data(str[i]);		// data write
	}
}

void LCD_xy(unsigned char col, unsigned char row){
	LCD_com(0x80 | (row+col * 0x40));	// DDRAM (AC6~AC0) 0x40값
	// DDRAM 셋 = 0x80
	// row=1, 칸(가료) = 첫번쨰 칸
	// col=0, 줄(세로) = 첫번째 줄, col=1(두번째줄), 우선곱셈
	// 0x00=첫번째 줄 첫번지, 0x40=두번째 줄 첫번지
} 

void LCD_clear(){
	LCD_com(0x01);
}
//// lcd init end