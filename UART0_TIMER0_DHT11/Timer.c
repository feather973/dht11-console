﻿/*
 * Timer.c
 *
 * Created: 2020-08-07 오후 4:23:28
 *  Author: kccistc
 */ 
#include <avr/interrupt.h>

void timer0_init(void){
	// 분주비 /1024
	// 16000000 / 1024 --> 16000(16KHz)	
	// 256 / 16000 --> 0.016 sec --> 16ms 마다 OVF 인터럽트 발생
	
	TCCR0 |= (1<< CS02) | (1<< CS01) | (1<< CS00);
	TIMSK |= (1<< TOIE0);	// TIMER0 OVERFLOW INT 허용
}